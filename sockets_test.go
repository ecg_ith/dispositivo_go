package main

import "testing"
import "fmt"
import "net"
import "time"

func TestRequestECG(t *testing.T) {
	path := "/tmp/TestRequestECG.sock"
	ln, err := net.Listen("unix", path)
	if err != nil {
		fmt.Println(err)
		t.FailNow()
	}
	defer ln.Close()

	err = requestECG(path)
	if err != nil {
		fmt.Println(err)
		t.FailNow()
	}

	conn, err := ln.Accept()
	if err != nil {
		fmt.Println(err)
		t.FailNow()
	}
	buf := make([]byte, 4096)
	n, err := conn.Read(buf)
	if err != nil {
		fmt.Println(err)
		t.FailNow()
	}
	b := buf[0:n]
	if string(b) != "Request:ECG" {
		fmt.Println(err)
		t.FailNow()
	}
}

func TestForward(t *testing.T) {
	path := "/tmp/TestForward.sock"
	c := make(chan []byte)
	go forward(path, c)

	time.Sleep(time.Millisecond)
	conn, err := net.Dial("unix", path)
	if err != nil {
		fmt.Println(err)
		t.FailNow()
	}
	defer conn.Close()

	_, err = conn.Write([]byte("EstoEsUnConjuntoDeDatos"))
	if err != nil {
		fmt.Println(err)
		t.FailNow()
	}
	time.Sleep(time.Millisecond)
	s := <-c
	if string(s) != "EstoEsUnConjuntoDeDatos" {
		t.FailNow()
	}
}
