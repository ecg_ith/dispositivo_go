package main

import (
	"log"
	"net"
	"os"
	"time"
)

const (
	socketPathRequestECG = "/tmp/Requests.sock"
	socketPathECGs       = "/tmp/ECGs.sock"
	socketPathAlerts     = "/tmp/Alerts.sock"
)

// requestECG forwards the request to path
func requestECG(path string) error {
	conn, err := net.Dial("unix", path)
	if err != nil {
		return err
	}
	defer conn.Close()

	_, err = conn.Write([]byte("REQUEST:ECG"))
	if err != nil {
		return err
	}
	return nil
}

// forward listen for incoming data in path and forwards it to the channel c
func forward(path string, c chan []byte) {
	for {
		os.Remove(path)
		ln, err := net.Listen("unix", path)
		if err != nil {
			log.Println(err)
			time.Sleep(time.Second * 10)
			continue
		}

		for {
			conn, err := ln.Accept()
			if err != nil {
				log.Println(err)
				break
			}
			go func(conn net.Conn) {
				for {
					buf := make([]byte, 4096)
					n, err := conn.Read(buf)
					if err != nil {
						return
					}
					c <- buf[0:n]
				}
			}(conn)
		}
		time.Sleep(time.Second * 10)
	}
}
