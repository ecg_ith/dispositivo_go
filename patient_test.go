package main

import "testing"
import "bytes"
import "time"

func TestUpload(t *testing.T) {
	p := newPatient()
	buf := bytes.NewBuffer(nil)
	go p.upload(buf)

	p.ECGs <- []byte("EstoEsUnElectroCardioGrama")
	time.Sleep(time.Millisecond)
	s, _ := buf.ReadString(0)
	if s != "Response:ECG:EstoEsUnElectroCardioGrama" {
		t.FailNow()
	}

	p.Alerts <- []byte("EstoEsUnaAlerta")
	time.Sleep(time.Millisecond)
	s, _ = buf.ReadString(0)
	if s != "Alert:ECG:EstoEsUnaAlerta" {
		t.FailNow()
	}

	p.Errors <- []byte("EstoEsUnError")
	time.Sleep(time.Millisecond)
	s, _ = buf.ReadString(0)
	if s != "Error:EstoEsUnError" {
		t.FailNow()
	}
}
