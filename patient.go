package main

//import "github.com/gorilla/websocket"
import "bytes"
import "io"
import "github.com/zhouhui8915/go-socket.io-client"

type patient struct {
	//Conn   *websocket.Conn
	Client *socketio_client.Client
	ECGs   chan []byte
	Alerts chan []byte
	Errors chan []byte
}

func newPatient() *patient {
	return &patient{
		ECGs:   make(chan []byte),
		Alerts: make(chan []byte),
		Errors: make(chan []byte)}
}

func (p *patient) upload(w io.Writer) {
	for {
		select {
		case data := <-p.ECGs:
			p.Client.Emit(string(bytes.Join([][]byte{[]byte("RESPONSE:ECG:"), data}, nil)))
			// _, err := w.Write(bytes.Join([][]byte{[]byte("RESPONSE:ECG:"), data}, nil))
			// if err != nil {
			// 	p.ECGs <- data
			// 	return
			// }
		case data := <-p.Alerts:
			p.Client.Emit(string(bytes.Join([][]byte{[]byte("ALERT:ECG:"), data}, nil)))
			// _, err := w.Write(bytes.Join([][]byte{[]byte("ALERT:ECG:"), data}, nil))
			// if err != nil {
			// 	p.Alerts <- data
			// 	return
			// }
		case data := <-p.Errors:
			p.Client.Emit(string(bytes.Join([][]byte{[]byte("ERROR:"), data}, nil)))
			// _, err := w.Write(bytes.Join([][]byte{[]byte("ERROR:"), data}, nil))
			// if err != nil {
			// 	p.Errors <- data
			// 	return
			// }
		}
	}
}
