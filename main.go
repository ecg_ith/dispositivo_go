package main

import (
	"log"
	"time"

	//"github.com/gorilla/websocket"
	"github.com/zhouhui8915/go-socket.io-client"
)

const (
	//addr = "ws://remotehost:9911"
	addr = "http://localhost:8080"
)

var (
	p *patient
	//dialer *websocket.Dialer
)

func main() {
	p = newPatient()
	//dialer = &websocket.Dialer{}

	go forward(socketPathECGs, p.ECGs)
	go forward(socketPathAlerts, p.Alerts)

	for {
		run()
		log.Println("Connection refused. Retrying in 10 seconds...")
		time.Sleep(time.Second * 10)
	}
}

func run() {
	// {
	// 	conn, _, err := dialer.Dial(addr, nil)
	// 	if err != nil {
	// 		return
	// 	}
	// 	p.Conn = conn
	// }
	// defer p.Conn.Close()
	{
		opts := &socketio_client.Options{
			Transport: "websocket",
			Query:     make(map[string]string)}
		opts.Query["patientId"] = "edf97d76-630e-4d2d-994c-587097d2c35d"
		client, err := socketio_client.NewClient(addr, opts)
		if err != nil {
			log.Printf("NewClient error: %v\n", err)
			return
		}
		p.Client = client
	}

	connectionAlive := true

	p.Client.On("error", func() {
		log.Printf("on error\n")
	})
	p.Client.On("connection", func() {
		log.Printf("on connect\n")
	})
	p.Client.On("message", func(msg string) {
		log.Printf("on message:%v\n", msg)
	})
	p.Client.On("disconnection", func() {
		log.Printf("on disconnect\n")
		connectionAlive = false
	})
	p.Client.On("REQUEST:ECG", func() {
		err := requestECG(socketPathRequestECG)
		if err != nil {
			p.Errors <- []byte("ECG:" + err.Error())
		}
	})

	p.Client.Emit("REQUEST:AUTH:edf97d76-630e-4d2d-994c-587097d2c35d:")

	for {
		if !connectionAlive {
			return
		}
	}

	//go p.upload(&uploader{p.Conn})

	// for {
	// 	_, msg, err := p.Conn.ReadMessage()
	// 	if err != nil {
	// 		return
	// 	}
	// 	switch string(msg) {
	// 	case "REQUEST:ECG":
	// 		err := requestECG(socketPathRequestECG)
	// 		if err != nil {
	// 			p.Errors <- []byte("ECG:" + err.Error())
	// 		}
	// 	}
	// }
}
